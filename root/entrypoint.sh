#!/usr/bin/env sh

err() {
    echo "ERROR: $1"
    exit 1
}

# Check if all needed variables are set and exit if not
[ -z "$LDAP_DOMAIN" ] && err "Variable LDAP_DOMAIN is undefined but needed"
[ -z "$LDAP_BASE" ] && err "Variable LDAP_BASE is undefined but needed"
[ -z "$LDAP_USER_BASE" ] && err "Variable LDAP_USER_BASE is undefined but needed"
[ -z "$LDAP_GROUP_BASE" ] && err "Variable LDAP_GROUP_BASE is undefined but needed"


if [ -n "$LDAP_DOMAIN" ] && [ -z "$LDAP_URL" ]; then
    export LDAP_URL="ldap://$LDAP_DOMAIN"
    echo "LDAP Domain is set but URL is not. Using domain to build URL: $LDAP_URL"
fi

if [ -n "$LDAP_EXTRA_CONF_FILE" ] && [ -f "$LDAP_EXTRA_CONF_FILE" ]; then
    export LDAP_EXTRA_CONF="$(cat "$LDAP_EXTRA_CONF_FILE")"
fi

# Set internal debug level variable
[ -n "$DEBUG" ] && export _LDAP_DEBUG="debug_level = $DEBUG"

# Set bind credentials
if [ -n "$LDAP_BIND" ]; then
    [ -z "$LDAP_BIND_PASSWD" ] && err "bind dn is set but no password was provided"

    export _LDAP_BIND="ldap_default_bind_dn = $LDAP_BIND"
    export _LDAP_BIND_PASSWD="ldap_default_authtok = $LDAP_BIND_PASSWD"
fi

# Render the SSSD Config if no custom file is provided
if [ ! -f "$SSSD_CONF_FILE" ]; then
    [ ! -f "$SSSD_CONF_FILE_TEMPLATE" ] && err "SSSD config file template could not be found"
    envsubst < "$SSSD_CONF_FILE_TEMPLATE" > "$SSSD_CONF_FILE"
fi

chown root:root "$SSSD_CONF_FILE"
chmod 0600 "$SSSD_CONF_FILE"

if [ -n "$SSHD_ONLY_SFTP" ]; then
    export SSHD_FORCE_COMMAND="ForceCommand /usr/lib/openssh/sftp-server"
fi

if [ -n "$SSHD_EXTRA_CONF_FILE" ] && [ -f "$SSHD_EXTRA_CONF_FILE" ]; then
    export SSHD_EXTRA_CONF="$(cat "$SSHD_EXTRA_CONF_FILE")"
fi

if [ ! -f "$SSHD_CONF_FILE" ]; then
    [ ! -f "$SSHD_CONF_FILE_TEMPLATE" ] && err "SSHD config file template could not be found"
    envsubst < "$SSHD_CONF_FILE_TEMPLATE" > "$SSHD_CONF_FILE"
fi

mkdir -p /run/sshd

/usr/sbin/sssd -i &
/usr/sbin/sshd -D -e
